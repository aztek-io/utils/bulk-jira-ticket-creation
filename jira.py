#!/usr/bin/env python
# Written by: Robert J.

import os
import logging
import sys
import boto3
import json
import requests
import re

from requests.auth import HTTPBasicAuth as auth

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Boto3 Configs #####################
#######################################

s3 = boto3.client('s3')

#######################################
### Global Vars #######################
#######################################

JIRA_BASE_URL   = os.environ.get('JIRA_BASE_URL', 'http://localhost:8080')
USERNAME        = os.environ['USERNAME']
PASSWORD        = os.environ['PASSWORD']

#######################################
### Main Function #####################
#######################################

def main(event, context):
    for record in event['Records']:
        payload = get_payload(
            bucket=record['s3']['bucket']['name'],
            s3_object=record['s3']['object']['key']
        )

        generate_ticket(payload)


#######################################
### Generic Functions #################
#######################################

def fatal(message, code=1):
    logger.critical(message)
    sys.exit(code)


def get_payload(bucket, s3_object):
    logger.info('Pulling Data from s3. for {}/{}'.format(bucket, s3_object))

    resp = s3.get_object(
        Bucket=bucket,
        Key=s3_object
    )

    body = resp['Body'].read()

    logger.info('Body: {}'.format(json.dumps(json.loads(body), indent=4)))

    return json.dumps(json.loads(body))


def generate_ticket(payload):
    headers = {
        "Content-Type": "application/json"
    }

    try:
        req = requests.post(
            JIRA_BASE_URL + '/rest/api/2/issue/',
            data=payload,
            headers=headers,
            auth=auth(USERNAME, PASSWORD),
            timeout=3
        )
    except requests.exceptions.Timeout as e:
        fatal("Server connection failed: {}".format(e))
    except requests.exceptions.RequestException as e:
        fatal("Request failed: {} {}".format(e.status_code, e.reason))

    if not re.match('2\d{2}', str(req.status_code)):
        fatal(
            "Non 200 status code: {}\nResponse Headers: {}\nResponse Text: {}".format(
                req.status_code,
                req.headers,
                json.dumps(req.text, indent=4)
            ),
            code=255
        )


#######################################
### Execution #########################
#######################################

def lambda_handler(event, context):
    main(event, context)


if __name__ == '__main__':
    stream = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    stream.setFormatter(formatter)

    logger.addHandler(stream)

    event = {
        "Records": [
            {
                "eventName": "ObjectCreated:Put",
                "s3": {
                    "bucket": {
                        "name": "jira.objects",
                    },
                    "object": {
                        "key": "test.json"
                    }
                }
            }
        ]
    }

    main(event, None)

