#!/usr/bin/env python

import logging
import sys
import os
import json
from urllib.parse import quote

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Global Vars #######################
#######################################

PARENT      = os.environ.get('PARENT', None)
PROJECT     = os.environ.get('PROJECT', 'TEST')
ASSIGNEE    = os.environ.get('ASSIGNEE', None)
ISSUE_TYPE  = os.environ.get('ISSUE_TYPE', 'Story')

#######################################
### Main Function #####################
#######################################

def main(filename):
    data = get_data(filename)

    for key in data.keys():
        for value in data[key]:
            payload = generate_payload(key, value)

            write_to_file(payload, key, value)


#######################################
### Program Specific Functions ########
#######################################

def get_data(filename):
    with open(filename, 'r') as json_file:
        data = json.loads(json_file.read())

        return data


def generate_payload(key, value, parent_ticket=PARENT, assignee=ASSIGNEE, issuetype=ISSUE_TYPE):
    payload = {
        "fields": {
            "project": {
                "key": PROJECT
            },
            "summary": '{} - {}'.format(key, value),
            "description": 'This ticket is to track work for the following aws resources:\n\n{}\n\nIAM permissions need to be restricted.'.format(value),
            "issuetype": {
                "name": issuetype
            }
        }
    }

    if parent_ticket:
        logger.info('Appending Parent: {}'.format(parent_ticket))
        payload['fields'].update(
            {
                "parent": {
                    "key": parent_ticket
                }
            }
        )

        logger.info('Ensuring Issue Type is "Sub-Task" since $PARENT is set.')
        payload['fields']['issuetype'].update(
            {
                "name": "Sub-task"
            }
        )

    if assignee:
        logger.info('Appending Assignee: {}'.format(assignee))
        payload['fields'].update(
            {
                "assignee": {
                    "name": assignee
                }
            }
        )

    logger.info('Payload: {}'.format(json.dumps(payload, indent=4)))

    return payload


def write_to_file(payload, key, value, directory='./json/'):
    if not os.path.isdir(directory):
        logger.info('Creating directory: {}'.format(directory))
        os.makedirs(directory)

    filename = directory + quote(
        '{}-{}.json'.format(
            key.replace(' ', '-'),
            value.replace(' ', '-')
        )
    )

    logger.info('Writing to {}.'.format(filename))

    f = open(filename, "w")
    f.write(json.dumps(payload, indent=4))
    f.close()


if __name__ == '__main__':
    stream = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    stream.setFormatter(formatter)

    logger.addHandler(stream)

    filename = 'tickets.json'

    main(filename)
